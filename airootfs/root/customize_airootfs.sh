#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

ROOT_PASSWORD='$6$bOVpyLIvp0wYJnl/$.xJAeXSwzCVK1HGuEvDLm61Gx8u49eX0opQ8OqQ1Z7TfW2eJWuYrakmFPEIVpZQSTzkMnCJm5hxNxQRhOZRR41'
sed -i "s#^root:.*\(\:.*\:.*\:.*\:.*\:.*\:.*\:\)#root:${ROOT_PASSWORD}\1#" /etc/shadow

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

# Since Tor will only work if system time is set correctly, we enable systemd-timesyncd.
systemctl enable pacman-init.service choose-mirror.service systemd-timesyncd.service
systemctl set-default multi-user.target
