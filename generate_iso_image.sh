#!/bin/sh

readonly DISTRIBUTION=`stat --format='%n' packages/x86_64/*.db | sed --silent 's#.*/\(.*\).db#\1#p'`
readonly BRANCH=`git rev-parse --abbrev-ref HEAD`

sudo mount --bind -o ro packages /opt/${DISTRIBUTION}/packages

if [ ! -z ${DISTRIBUTION} -a ! -z ${BRANCH} -a -f build.sh ] ; then
	sudo rm -v work/build.make_*
	sudo rm -v out/${DISTRIBUTION}-${BRANCH}-*.iso
	sudo ./build.sh -v -N "${DISTRIBUTION}-${BRANCH}" -w work -o out
else
	echo "ERROR: Please verify that this directory is correctly set up."
fi

sudo umount /opt/${DISTRIBUTION}/packages
